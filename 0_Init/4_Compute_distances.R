# Compute distances between pairs of sites 

# Import packages
library(sf)

# Working directory
wd <- "D:/Mes Donnees/EboHealth/bat-move"
setwd(wd)

# Delete/create folders
unlink("0_Init/Distances/", recursive=TRUE)
dir.create("0_Init/Distances", recursive=TRUE)

# Compute distance between all sites (foraging & roosts)
lr <- list.files(paste0("0_Init/Rasters/Roost/"))      # List of files
for(i in 1:length(lr)){
  
  id <- substr(lr[i], 1, nchar(lr[i])-4)
  shpr <- st_read(paste0("0_Init/SHPs/Roost/", id ,".shp"), quiet = TRUE)
  shpf <- st_read(paste0("0_Init/SHPs/Foraging/", id ,".shp"), quiet = TRUE)
  
  shp <- rbind(shpr[,c(1,6)], shpf[,c(1,6)])
  
  d <- as.matrix(st_distance(shp))
  rownames(d) <- shp$ID
  colnames(d) <- shp$ID
  
  write.csv2(d, paste0("0_Init/Distances/", id, ".csv"))
  
  print(c(id, mean(d[1:80,1:80]), mean(d[-c(1:80),-c(1:80)])))
  
}




