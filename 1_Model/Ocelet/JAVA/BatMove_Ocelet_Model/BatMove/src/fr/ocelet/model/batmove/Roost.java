package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Roostpoint;
import fr.ocelet.runtime.entity.AbstractEntity;
import fr.ocelet.runtime.entity.Hproperty;
import fr.ocelet.runtime.geom.ocltypes.Point;
import fr.ocelet.runtime.ocltypes.DateTime;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;

@SuppressWarnings("all")
public class Roost extends AbstractEntity {
  public void setId(final String id) {
    setProperty("id",id);
  }
  
  public String getId() {
    return getProperty("id");
  }
  
  public void setNo_bats(final Integer no_bats) {
    setProperty("no_bats",no_bats);
  }
  
  public Integer getNo_bats() {
    return getProperty("no_bats");
  }
  
  public void setLocation(final Point location) {
    setProperty("location",location);
  }
  
  public Point getLocation() {
    return getProperty("location");
  }
  
  public void setDx(final Double dx) {
    setProperty("dx",dx);
  }
  
  public Double getDx() {
    return getProperty("dx");
  }
  
  public void setDy(final Double dy) {
    setProperty("dy",dy);
  }
  
  public Double getDy() {
    return getProperty("dy");
  }
  
  public void setKm_nf(final KeyMap<Integer, Double> km_nf) {
    setProperty("km_nf",km_nf);
  }
  
  public KeyMap<Integer, Double> getKm_nf() {
    return getProperty("km_nf");
  }
  
  public void setNfmax(final Double nfmax) {
    setProperty("nfmax",nfmax);
  }
  
  public Double getNfmax() {
    return getProperty("nfmax");
  }
  
  public void setRoost_loc(final List<Roostpoint> roost_loc) {
    setProperty("roost_loc",roost_loc);
  }
  
  public List<Roostpoint> getRoost_loc() {
    return getProperty("roost_loc");
  }
  
  public void updateRoostpoint(final DateTime tstamp) {
    final Roostpoint rp = new Roostpoint();
    rp.setGeom(Point.xy(Double.valueOf(this.getLocation().getX()), Double.valueOf(this.getLocation().getY())));
    rp.setTimestamp(tstamp);
    rp.setRoost_id(this.getId());
    rp.setNb_bats(this.getNo_bats());
    this.getRoost_loc().add(rp);
  }
  
  public Roost() {
    super();
    defProperty("id",new Hproperty<String>());
    setId(new String());
    defProperty("no_bats",new Hproperty<Integer>());
    setNo_bats(new Integer("0"));
    defProperty("location",new Hproperty<Point>());
    setLocation(new Point());
    defProperty("dx",new Hproperty<Double>());
    setDx(new Double("0"));
    defProperty("dy",new Hproperty<Double>());
    setDy(new Double("0"));
    defProperty("km_nf",new Hproperty<KeyMap<Integer, Double>>());
    setKm_nf(new KeyMap<Integer, Double>());
    defProperty("nfmax",new Hproperty<Double>());
    setNfmax(new Double("0"));
    defProperty("roost_loc",new Hproperty<List<Roostpoint>>());
    setRoost_loc(new List<Roostpoint>());
  }
}
