package fr.ocelet.model.batmove;

import fr.ocelet.runtime.entity.AbstractEntity;
import fr.ocelet.runtime.entity.Hproperty;

@SuppressWarnings("all")
public class Roost_shp extends AbstractEntity {
  public void setDateShow(final String dateShow) {
    setProperty("dateShow",dateShow);
  }
  
  public String getDateShow() {
    return getProperty("dateShow");
  }
  
  public void setDateHide(final String dateHide) {
    setProperty("dateHide",dateHide);
  }
  
  public String getDateHide() {
    return getProperty("dateHide");
  }
  
  public void setRoost_id(final String roost_id) {
    setProperty("roost_id",roost_id);
  }
  
  public String getRoost_id() {
    return getProperty("roost_id");
  }
  
  public void setLon(final Integer lon) {
    setProperty("lon",lon);
  }
  
  public Integer getLon() {
    return getProperty("lon");
  }
  
  public void setLat(final Integer lat) {
    setProperty("lat",lat);
  }
  
  public Integer getLat() {
    return getProperty("lat");
  }
  
  public void setNb_bats(final Integer nb_bats) {
    setProperty("nb_bats",nb_bats);
  }
  
  public Integer getNb_bats() {
    return getProperty("nb_bats");
  }
  
  public Roost_shp() {
    super();
    defProperty("dateShow",new Hproperty<String>());
    setDateShow(new String());
    defProperty("dateHide",new Hproperty<String>());
    setDateHide(new String());
    defProperty("roost_id",new Hproperty<String>());
    setRoost_id(new String());
    defProperty("lon",new Hproperty<Integer>());
    setLon(new Integer("0"));
    defProperty("lat",new Hproperty<Integer>());
    setLat(new Integer("0"));
    defProperty("nb_bats",new Hproperty<Integer>());
    setNb_bats(new Integer("0"));
  }
}
