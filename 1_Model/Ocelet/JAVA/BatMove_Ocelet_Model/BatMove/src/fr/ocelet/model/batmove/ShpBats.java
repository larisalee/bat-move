package fr.ocelet.model.batmove;

import fr.ocelet.datafacer.InputDataRecord;
import fr.ocelet.datafacer.ocltypes.Csvfile;
import fr.ocelet.model.batmove.Bats_shp;
import fr.ocelet.runtime.entity.Entity;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.LinkedHashMap;

@SuppressWarnings("all")
public class ShpBats extends Csvfile {
  public ShpBats() {
    super("output/Bats.csv");
  }
  
  public List<Bats_shp> readAllBats_shp() {
    List<Bats_shp> _elist = new List<Bats_shp>();
    for (InputDataRecord _record : this) {
      _elist.add(createBats_shpFromRecord(_record));
     }
    resetIterator();
    return _elist;
  }
  
  public List<Bats_shp> readAll() {
    return readAllBats_shp();
  }
  
  public KeyMap<Object, Bats_shp> readToKeyMap(final String keyproperty) {
    KeyMap<Object,Bats_shp> _edf_km = new KeyMap<Object,Bats_shp>();
    for (InputDataRecord _record : this) {
    	Bats_shp _en_ti_ty = createBats_shpFromRecord(_record);
    	Object _the_key_ = _en_ti_ty.getProperty(keyproperty);
      _edf_km.put(_the_key_,_en_ti_ty);
    }
    resetIterator();
    return _edf_km;
  }
  
  public Bats_shp createBats_shpFromRecord(final InputDataRecord _rec) {
                      	    Bats_shp _entity = new Bats_shp();
    _entity.setProperty("gid",readInteger("gid"));
    _entity.setProperty("dateShow",readString("start"));
    _entity.setProperty("dateHide",readString("end"));
    _entity.setProperty("id",readInteger("id_bat"));
    _entity.setProperty("activity",readString("activity"));
    _entity.setProperty("roost_id",readString("roost_id"));
    _entity.setProperty("forage_id",readString("forage_id"));
    _entity.setProperty("lon",readInteger("lon"));
    _entity.setProperty("lat",readInteger("lat"));
    return _entity;
  }
  
  public LinkedHashMap<String, String> getMatchdef() {
    LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
    hm.put("gid","java.lang.Integer");
    hm.put("start","java.lang.String");
    hm.put("end","java.lang.String");
    hm.put("id_bat","java.lang.Integer");
    hm.put("activity","java.lang.String");
    hm.put("roost_id","java.lang.String");
    hm.put("forage_id","java.lang.String");
    hm.put("lon","java.lang.Integer");
    hm.put("lat","java.lang.Integer");
    return hm;
  }
  
  public String headerString() {
    StringBuffer sb = new StringBuffer();
    sb.append("gid");
    sb.append(separator);                     
    sb.append("start");
    sb.append(separator);                     
    sb.append("end");
    sb.append(separator);                     
    sb.append("id_bat");
    sb.append(separator);                     
    sb.append("activity");
    sb.append(separator);                     
    sb.append("roost_id");
    sb.append(separator);                     
    sb.append("forage_id");
    sb.append(separator);                     
    sb.append("lon");
    sb.append(separator);                     
    sb.append("lat");
    return sb.toString();
  }
  
  public String propsString(final Entity _entity) {
    StringBuffer sb = new StringBuffer();
    sb.append(_entity.getProperty("gid").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("dateShow").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("dateHide").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("id").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("activity").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("roost_id").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("forage_id").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("lon").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("lat").toString());
    return sb.toString();
  }
}
