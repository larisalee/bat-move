package fr.ocelet.model.batmove;

import fr.ocelet.runtime.entity.AbstractEntity;
import fr.ocelet.runtime.entity.Hproperty;

@SuppressWarnings("all")
public class Bats_shp extends AbstractEntity {
  public void setGid(final Integer gid) {
    setProperty("gid",gid);
  }
  
  public Integer getGid() {
    return getProperty("gid");
  }
  
  public void setDateShow(final String dateShow) {
    setProperty("dateShow",dateShow);
  }
  
  public String getDateShow() {
    return getProperty("dateShow");
  }
  
  public void setDateHide(final String dateHide) {
    setProperty("dateHide",dateHide);
  }
  
  public String getDateHide() {
    return getProperty("dateHide");
  }
  
  public void setId(final Integer id) {
    setProperty("id",id);
  }
  
  public Integer getId() {
    return getProperty("id");
  }
  
  public void setActivity(final String activity) {
    setProperty("activity",activity);
  }
  
  public String getActivity() {
    return getProperty("activity");
  }
  
  public void setRoost_id(final String roost_id) {
    setProperty("roost_id",roost_id);
  }
  
  public String getRoost_id() {
    return getProperty("roost_id");
  }
  
  public void setForage_id(final String forage_id) {
    setProperty("forage_id",forage_id);
  }
  
  public String getForage_id() {
    return getProperty("forage_id");
  }
  
  public void setLon(final Integer lon) {
    setProperty("lon",lon);
  }
  
  public Integer getLon() {
    return getProperty("lon");
  }
  
  public void setLat(final Integer lat) {
    setProperty("lat",lat);
  }
  
  public Integer getLat() {
    return getProperty("lat");
  }
  
  public Bats_shp() {
    super();
    defProperty("gid",new Hproperty<Integer>());
    setGid(new Integer("0"));
    defProperty("dateShow",new Hproperty<String>());
    setDateShow(new String());
    defProperty("dateHide",new Hproperty<String>());
    setDateHide(new String());
    defProperty("id",new Hproperty<Integer>());
    setId(new Integer("0"));
    defProperty("activity",new Hproperty<String>());
    setActivity(new String());
    defProperty("roost_id",new Hproperty<String>());
    setRoost_id(new String());
    defProperty("forage_id",new Hproperty<String>());
    setForage_id(new String());
    defProperty("lon",new Hproperty<Integer>());
    setLon(new Integer("0"));
    defProperty("lat",new Hproperty<Integer>());
    setLat(new Integer("0"));
  }
}
