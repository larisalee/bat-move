package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Roost_shp;
import fr.ocelet.model.batmove.Roostpoint;
import fr.ocelet.model.batmove.ShpRoost;
import fr.ocelet.runtime.ocltypes.List;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class Roostpoints {
  public void saveToCsv(final List<Roostpoint> l_roostp, final String csvfilename) {
    int _size = l_roostp.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      final List<Roost_shp> l_roostshp = new List<Roost_shp>();
      final ShpRoost shproost = new ShpRoost();
      shproost.remove();
      shproost.setFileName(csvfilename);
      int _size_1 = l_roostp.size();
      boolean _greaterEqualsThan = (_size_1 >= 1);
      if (_greaterEqualsThan) {
        int _size_2 = l_roostp.size();
        int _minus = (_size_2 - 1);
        IntegerRange _upTo = new IntegerRange(1, _minus);
        for (final Integer i : _upTo) {
          {
            final Roostpoint rpa = l_roostp.get(((i).intValue() - 1));
            final Roostpoint rpb = l_roostp.get((i).intValue());
            final Roost_shp roost_shp = new Roost_shp();
            roost_shp.setLat(Integer.valueOf(Long.valueOf(Math.round(rpa.getGeom().getY())).intValue()));
            roost_shp.setLon(Integer.valueOf(Long.valueOf(Math.round(rpa.getGeom().getX())).intValue()));
            roost_shp.setDateShow(rpa.getTimestamp().toString());
            roost_shp.setDateHide(rpb.getTimestamp().addHours((-12)).toString());
            roost_shp.setRoost_id(rpa.getRoost_id());
            roost_shp.setNb_bats(rpa.getNb_bats());
            l_roostshp.add(roost_shp);
          }
        }
      }
      shproost.append(l_roostshp);
    }
  }
  
  public Roostpoints() {
    super();
  }
}
