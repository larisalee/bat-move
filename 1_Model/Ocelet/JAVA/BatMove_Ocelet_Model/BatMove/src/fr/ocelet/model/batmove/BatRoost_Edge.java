package fr.ocelet.model.batmove;

import com.google.common.base.Objects;
import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.Forage;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.model.batmove.Specialrandoms;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import fr.ocelet.runtime.relation.InteractionGraph;
import fr.ocelet.runtime.relation.OcltEdge;
import fr.ocelet.runtime.relation.OcltRole;
import fr.ocelet.runtime.relation.aggregops.Sum;
import java.util.Collection;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class BatRoost_Edge extends OcltEdge {
  private Bat bat;
  
  public void setBat(final Bat bat) {
    this.bat = bat;
  }
  
  public Bat getBat() {
    return this.bat;
  }
  
  private Roost roost;
  
  public void setRoost(final Roost roost) {
    this.roost = roost;
  }
  
  public Roost getRoost() {
    return this.roost;
  }
  
  public BatRoost_Edge(final InteractionGraph igr, final Bat first, final Roost second) {
    super(igr);
    bat=first;
    roost=second;
  }
  
  public OcltRole getRole(final int i) {
    if (i==0) return bat;
    else if (i==1) return roost;
    else return null;
  }
  
  public void choose() {
    String _roost_id = this.bat.getRoost_id();
    String _id = this.roost.getId();
    boolean _tripleNotEquals = (_roost_id != _id);
    if (_tripleNotEquals) {
      this.bat.setRoost_id(this.roost.getId());
    }
    double _random = Math.random();
    this.bat.setNo_daysRoost(Integer.valueOf(Double.valueOf((_random * 10)).intValue()));
  }
  
  public void getRoostLocation() {
    String _roost_id = this.bat.getRoost_id();
    String _id = this.roost.getId();
    boolean _equals = Objects.equal(_roost_id, _id);
    if (_equals) {
      this.bat.setRoost(this.roost.getLocation());
    }
  }
  
  public void sumBats() {
    this.roost.setNo_bats(Integer.valueOf(1));
  }
  
  public void _agr_sumBats() {
    this.roost.setAgregOp("no_bats",new Sum<Integer>(),false);
  }
  
  public void printedges() {
    String _roost_id = this.bat.getRoost_id();
    String _id = this.roost.getId();
    boolean _equals = Objects.equal(_roost_id, _id);
    if (_equals) {
      Integer _id_1 = this.bat.getId();
      String _plus = ("bat_" + _id_1);
      String _plus_1 = (_plus + " <----> roost_");
      String _id_2 = this.roost.getId();
      String _plus_2 = (_plus_1 + _id_2);
      InputOutput.<String>println(_plus_2);
    } else {
      Integer _id_3 = this.bat.getId();
      String _plus_3 = (_id_3 + " <-  ->");
      String _id_4 = this.roost.getId();
      String _plus_4 = (_plus_3 + _id_4);
      InputOutput.<String>println(_plus_4);
    }
  }
  
  public void prepareForageList(final List<Forage> l_forage, final Specialrandoms sprand) {
    final List<String> l_potForage = new List<String>();
    int forage_sites = 5;
    double sum = 0d;
    KeyMap<Integer, Double> l_nf = this.roost.getKm_nf();
    Collection<Double> _values = l_nf.values();
    for (final Double vals : _values) {
      double _sum = sum;
      sum = (_sum + (vals).doubleValue());
    }
    final List<Integer> lrandomIndex = sprand.multinomial_i(forage_sites, l_nf, sum);
    for (int i = 0; (i < lrandomIndex.size()); i++) {
      Integer _get = lrandomIndex.get(i);
      String _plus = ("F_" + _get);
      l_potForage.add(_plus);
    }
    this.bat.getPlForagelist().addAll(l_potForage);
  }
}
