package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.BatRoost_Edge;
import fr.ocelet.model.batmove.Forage;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.model.batmove.Specialrandoms;
import fr.ocelet.runtime.ocltypes.List;
import fr.ocelet.runtime.relation.RoleSet;
import fr.ocelet.runtime.relation.impl.DiGraph;
import fr.ocelet.runtime.relation.impl.RoleSetImpl;
import java.util.Collection;
import java.util.HashSet;

@SuppressWarnings("all")
public class BatRoost extends DiGraph<BatRoost_Edge, Bat, Roost> {
  public BatRoost() {
    super();
  }
  
  public BatRoost_Edge connect(final Bat bat, final Roost roost) {
    if ((this.batSet == null) || (!this.batSet.contains(bat))) add(bat);
    if ((this.roostSet == null) || (!this.roostSet.contains(roost))) add(roost);
    BatRoost_Edge _gen_edge_ = new BatRoost_Edge(this,bat,roost);
    addEdge(_gen_edge_);
    return _gen_edge_;
  }
  
  public RoleSet<Bat> getLeftSet() {
    return batSet;
  }
  
  public RoleSet<Roost> getRightSet() {
    return roostSet;
  }
  
  public BatRoost getComplete() {
    return (BatRoost)super.getComplete();
  }
  
  public BatRoost_Edge createEdge(final Bat bat, final Roost roost) {
     return new BatRoost_Edge(this,bat,roost);
  }
  
  private RoleSet<Bat> batSet;
  
  public void setBatSet(final Collection<Bat> croles) {
    this.batSet=new RoleSetImpl<Bat>(croles);
  }
  
  public RoleSet<Bat> getBatSet() {
    return batSet;
  }
  
  private RoleSet<Roost> roostSet;
  
  public void setRoostSet(final Collection<Roost> croles) {
    this.roostSet=new RoleSetImpl<Roost>(croles);
  }
  
  public RoleSet<Roost> getRoostSet() {
    return roostSet;
  }
  
  public void add(final Bat role) {
    addBat(role);
  }
  
  public void remove(final Bat role) {
    removeBat(role);
  }
  
  public void addBat(final Bat role) {
    if (this.batSet == null) setBatSet( new HashSet<Bat>());
    this.batSet.addRole(role);
  }
  
  public void removeBat(final Bat role) {
    if (this.batSet != null) this.batSet.removeRole(role);
  }
  
  public void addAllBat(final Iterable<Bat> roles) {
    if (this.batSet == null) setBatSet( new HashSet<Bat>());
    this.batSet.addRoles(roles);
  }
  
  public void removeAllBat(final Iterable<Bat> roles) {
    if (this.batSet != null) this.batSet.removeRoles(roles);
  }
  
  public void addRoost(final Roost role) {
    if (this.roostSet == null) setRoostSet( new HashSet<Roost>());
    this.roostSet.addRole(role);
  }
  
  public void remove(final Roost role) {
    removeRoost(role);
  }
  
  public void add(final Roost role) {
    addRoost(role);
  }
  
  public void removeRoost(final Roost role) {
    if (this.roostSet != null) this.roostSet.removeRole(role);
  }
  
  public void addAllRoost(final Iterable<Roost> roles) {
    if (this.roostSet == null) setRoostSet( new HashSet<Roost>());
    this.roostSet.addRoles(roles);
  }
  
  public void removeAllRoost(final Iterable<Roost> roles) {
    if (this.roostSet != null) this.roostSet.removeRoles(roles);
  }
  
  public BatRoost batonroost() {
    BatRoost_batonroost _filter = new BatRoost_batonroost();
    super.addFilter(_filter);
    return this;
  }
  
  public BatRoost validRoost() {
    BatRoost_validRoost _filter = new BatRoost_validRoost();
    super.addFilter(_filter);
    return this;
  }
  
  public BatRoost monthlychange(final Integer dayOfMonth) {
    BatRoost_monthlychange _filter = new BatRoost_monthlychange(dayOfMonth);
    super.addFilter(_filter);
    return this;
  }
  
  public void choose() {
    beginTransaction();
    for(BatRoost_Edge _edg_ : this) {
      _edg_.choose();
    }
    endTransaction();
  }
  
  public void getRoostLocation() {
    beginTransaction();
    for(BatRoost_Edge _edg_ : this) {
      _edg_.getRoostLocation();
    }
    endTransaction();
  }
  
  public void sumBats() {
    beginTransaction();
    for(BatRoost_Edge _edg_ : this) {
      _edg_.sumBats();
     _edg_._agr_sumBats();
    }
    endTransaction();
  }
  
  public void printedges() {
    beginTransaction();
    for(BatRoost_Edge _edg_ : this) {
      _edg_.printedges();
    }
    endTransaction();
  }
  
  public void prepareForageList(final List<Forage> l_forage, final Specialrandoms sprand) {
    beginTransaction();
    for(BatRoost_Edge _edg_ : this) {
      _edg_.prepareForageList(l_forage,sprand);
    }
    endTransaction();
  }
}
