package fr.ocelet.model.batmove;

import fr.ocelet.runtime.geom.ocltypes.Point;
import fr.ocelet.runtime.ocltypes.DateTime;

@SuppressWarnings("all")
public class Batpoint {
  private Point geom;
  
  public void setGeom(final Point geom) {
    this.geom = geom;
  }
  
  public Point getGeom() {
    return this.geom;
  }
  
  private Integer gid;
  
  public void setGid(final Integer gid) {
    this.gid = gid;
  }
  
  public Integer getGid() {
    return this.gid;
  }
  
  private DateTime timestamp1;
  
  public void setTimestamp1(final DateTime timestamp1) {
    this.timestamp1 = timestamp1;
  }
  
  public DateTime getTimestamp1() {
    return this.timestamp1;
  }
  
  private DateTime timestamp2;
  
  public void setTimestamp2(final DateTime timestamp2) {
    this.timestamp2 = timestamp2;
  }
  
  public DateTime getTimestamp2() {
    return this.timestamp2;
  }
  
  private Integer id;
  
  public void setId(final Integer id) {
    this.id = id;
  }
  
  public Integer getId() {
    return this.id;
  }
  
  private String roost_id;
  
  public void setRoost_id(final String roost_id) {
    this.roost_id = roost_id;
  }
  
  public String getRoost_id() {
    return this.roost_id;
  }
  
  private String forageID;
  
  public void setForageID(final String forageID) {
    this.forageID = forageID;
  }
  
  public String getForageID() {
    return this.forageID;
  }
  
  private String activity;
  
  public void setActivity(final String activity) {
    this.activity = activity;
  }
  
  public String getActivity() {
    return this.activity;
  }
  
  public Batpoint() {
    super();
    geom = new Point();
    gid = new Integer("0");
    timestamp1 = new DateTime();
    timestamp2 = new DateTime();
    id = new Integer("0");
    roost_id = new String();
    forageID = new String();
    activity = new String();
  }
}
