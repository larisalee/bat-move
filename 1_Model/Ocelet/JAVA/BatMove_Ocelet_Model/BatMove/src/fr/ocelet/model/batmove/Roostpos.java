package fr.ocelet.model.batmove;

import fr.ocelet.datafacer.InputDataRecord;
import fr.ocelet.datafacer.OutputDataRecord;
import fr.ocelet.datafacer.ocltypes.Shapefile;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.runtime.entity.Entity;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.LinkedHashMap;

@SuppressWarnings("all")
public class Roostpos extends Shapefile {
  public Roostpos() {
    super("data/roost.shp","EPSG:32629");
  }
  
  public List<Roost> readAllRoost() {
    List<Roost> _elist = new List<Roost>();
    for (InputDataRecord _record : this) {
      _elist.add(createRoostFromRecord(_record));
     }
    resetIterator();
    return _elist;
  }
  
  public List<Roost> readAll() {
    return readAllRoost();
  }
  
  public KeyMap<Object, Roost> readToKeyMap(final String keyproperty) {
    KeyMap<Object,Roost> _edf_km = new KeyMap<Object,Roost>();
    for (InputDataRecord _record : this) {
    	Roost _en_ti_ty = createRoostFromRecord(_record);
    	Object _the_key_ = _en_ti_ty.getProperty(keyproperty);
      _edf_km.put(_the_key_,_en_ti_ty);
    }
    resetIterator();
    return _edf_km;
  }
  
  public Roost createRoostFromRecord(final InputDataRecord _rec) {
                      	    Roost _entity = new Roost();
    _entity.setProperty("location",readPoint("the_geom"));
    _entity.setProperty("id",readString("ID"));
    return _entity;
  }
  
  public LinkedHashMap<String, String> getMatchdef() {
    LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
    hm.put("the_geom","fr.ocelet.runtime.geom.ocltypes.Point");
    hm.put("ID","java.lang.String");
    return hm;
  }
  
  public List<Roost> readFilteredRoost(final String _filt) {
    setFilter(_filt);
    return readAllRoost();
  }
  
  public OutputDataRecord createRecord(final Entity ety) throws IllegalArgumentException {
    OutputDataRecord odr = createOutputDataRec();
    if (odr != null) {
    odr.setAttribute("the_geom",((Roost) ety).getLocation());
    odr.setAttribute("ID",((Roost) ety).getId());
    }
    return odr;
  }
}
