package fr.ocelet.model.batmove;

import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.Iterator;
import org.eclipse.xtext.xbase.lib.DoubleExtensions;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class Specialrandoms {
  public List<Integer> multinomial_i(final int n, final KeyMap<Integer, Double> kmweights, final double sum) {
    final List<Integer> lrandomIndex = new List<Integer>();
    IntegerRange _upTo = new IntegerRange(0, (n - 1));
    for (final Integer k : _upTo) {
      lrandomIndex.add(Integer.valueOf(0));
    }
    final List<Double> lrandom = new List<Double>();
    IntegerRange _upTo_1 = new IntegerRange(0, (n - 1));
    for (final Integer k_1 : _upTo_1) {
      double _random = Math.random();
      double _multiply = (_random * sum);
      lrandom.add(Double.valueOf(_multiply));
    }
    IntegerRange _upTo_2 = new IntegerRange(0, (n - 1));
    for (final Integer k_2 : _upTo_2) {
      {
        boolean done = false;
        final Iterator<Integer> keyIterator = kmweights.keySet().iterator();
        while ((keyIterator.hasNext() && (!done))) {
          {
            final Integer key = keyIterator.next();
            Double _get = lrandom.get((k_2).intValue());
            Double _get_1 = kmweights.get(key);
            double _minus = DoubleExtensions.operator_minus(_get, _get_1);
            lrandom.set((k_2).intValue(), Double.valueOf(_minus));
            Double _get_2 = lrandom.get((k_2).intValue());
            boolean _lessEqualsThan = ((_get_2).doubleValue() <= 0.0);
            if (_lessEqualsThan) {
              lrandomIndex.set((k_2).intValue(), key);
              done = true;
            }
          }
        }
      }
    }
    return lrandomIndex;
  }
  
  public Specialrandoms() {
    super();
  }
}
