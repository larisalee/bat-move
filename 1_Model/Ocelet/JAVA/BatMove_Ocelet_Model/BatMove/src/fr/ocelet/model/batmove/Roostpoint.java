package fr.ocelet.model.batmove;

import fr.ocelet.runtime.geom.ocltypes.Point;
import fr.ocelet.runtime.ocltypes.DateTime;

@SuppressWarnings("all")
public class Roostpoint {
  private Point geom;
  
  public void setGeom(final Point geom) {
    this.geom = geom;
  }
  
  public Point getGeom() {
    return this.geom;
  }
  
  private DateTime timestamp;
  
  public void setTimestamp(final DateTime timestamp) {
    this.timestamp = timestamp;
  }
  
  public DateTime getTimestamp() {
    return this.timestamp;
  }
  
  private String roost_id;
  
  public void setRoost_id(final String roost_id) {
    this.roost_id = roost_id;
  }
  
  public String getRoost_id() {
    return this.roost_id;
  }
  
  private Integer nb_bats;
  
  public void setNb_bats(final Integer nb_bats) {
    this.nb_bats = nb_bats;
  }
  
  public Integer getNb_bats() {
    return this.nb_bats;
  }
  
  public Roostpoint() {
    super();
    geom = new Point();
    timestamp = new DateTime();
    roost_id = new String();
    nb_bats = new Integer("0");
  }
}
