package fr.ocelet.model.batmove;

import com.google.common.base.Objects;
import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.runtime.relation.EdgeFilter;

@SuppressWarnings("all")
public class BatRoost_batonroost implements EdgeFilter<Bat, Roost> {
  public BatRoost_batonroost() {
    
  }
  
  public Boolean filter(final Bat bat, final Roost roost) {
    String _id = roost.getId();
    String _roost_id = bat.getRoost_id();
    return Boolean.valueOf(Objects.equal(_id, _roost_id));
  }
}
