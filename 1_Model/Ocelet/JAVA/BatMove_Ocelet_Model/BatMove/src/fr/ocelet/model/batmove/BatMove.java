package fr.ocelet.model.batmove;

import com.google.common.base.Objects;
import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.BatRoost;
import fr.ocelet.model.batmove.Batpoint;
import fr.ocelet.model.batmove.Batpoints;
import fr.ocelet.model.batmove.Forage;
import fr.ocelet.model.batmove.Foraging;
import fr.ocelet.model.batmove.RandomUnique;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.model.batmove.Roostpos;
import fr.ocelet.model.batmove.Specialrandoms;
import fr.ocelet.runtime.Miscutils;
import fr.ocelet.runtime.geom.ocltypes.Point;
import fr.ocelet.runtime.model.AbstractModel;
import fr.ocelet.runtime.model.NumericParameterImpl;
import fr.ocelet.runtime.model.Parameter;
import fr.ocelet.runtime.model.ParameterImpl;
import fr.ocelet.runtime.ocltypes.DateTime;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.HashMap;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class BatMove extends AbstractModel {
  public BatMove() {
    super("BatMove");
    Parameter<String> par_wd = new ParameterImpl<String>("wd","",true,"/C:/Users/localadmin/Test_M/GitLab_project/bat-move",null);
    addParameter(par_wd);
    wd = "/C:/Users/localadmin/Test_M/GitLab_project/bat-move";
    Parameter<String> par_filename = new ParameterImpl<String>("filename","",true,"",null);
    addParameter(par_filename);
    filename = "";
    Parameter<Integer> par_nbsimul = new NumericParameterImpl<Integer>("nbsimul","",true,0,null,null,null);
    addParameter(par_nbsimul);
    nbsimul = 0;
    Parameter<Integer> par_nbbats = new NumericParameterImpl<Integer>("nbbats","",true,0,null,null,null);
    addParameter(par_nbbats);
    nbbats = 0;
    Parameter<Integer> par_nbdays = new NumericParameterImpl<Integer>("nbdays","",true,0,null,null,null);
    addParameter(par_nbdays);
    nbdays = 0;
    Parameter<String> par_startDate = new ParameterImpl<String>("startDate","",true,"",null);
    addParameter(par_startDate);
    startDate = "";
    Parameter<Integer> par_outstep = new NumericParameterImpl<Integer>("outstep","",true,0,null,null,null);
    addParameter(par_outstep);
    outstep = 0;
  }
  
  public static void main(final String[] args) {
    BatMove model_BatMove = new BatMove();
    model_BatMove.setCmdLineArgs(args);
    if ((args.length > 0) && (args.length == model_BatMove.modParams.size())) {
       	HashMap<String,Object> parmap = model_BatMove.parseParams(args);
       	model_BatMove.simulate(parmap);
       } else model_BatMove.run_BatMove();
  }
  
  public void run_BatMove() {
    InputOutput.<String>println("Model BatMove ready to run");
    DateTime startTime = new DateTime();
    final Integer no_iter = this.nbsimul;
    IntegerRange _upTo = new IntegerRange(1, (no_iter).intValue());
    for (final Integer j : _upTo) {
      {
        Miscutils.createDir(((((this.wd + "/1_Model/Outputs/") + this.filename) + "/Sim_") + j));
        String SvPath = ((((this.wd + "/1_Model/Outputs/") + this.filename) + "/Sim_") + j);
        final double speed = 330d;
        Integer nbbat = this.nbbats;
        final double dmax = 10000d;
        DateTime start = DateTime.fromString("yyyy-MM-dd HH:mm", this.startDate);
        DateTime end = start.addDays((this.nbdays).intValue());
        DateTime now = start;
        final int tsstep = 1;
        final int nb_outst = (60 / (this.outstep).intValue());
        List<Integer> l_updt = new List<Integer>();
        int outstp = 0;
        IntegerRange _upTo_1 = new IntegerRange(0, (nb_outst - 1));
        for (final Integer st : _upTo_1) {
          {
            outstp = (st).intValue();
            l_updt.add(Integer.valueOf((outstp * (this.outstep).intValue())));
          }
        }
        final Roostpos roost = new Roostpos();
        roost.setFileName((((this.wd + "/0_Init/SHPs/Roost/") + this.filename) + ".shp"));
        List<Roost> l_roost = roost.readAll();
        int _size = l_roost.size();
        String _plus = ("number of roosts: " + Integer.valueOf(_size));
        InputOutput.<String>println(_plus);
        int nbroost = l_roost.size();
        final Foraging forage = new Foraging();
        forage.setFileName((((this.wd + "/0_Init/SHPs/Foraging/") + this.filename) + ".shp"));
        List<Forage> l_forage = forage.readAll();
        int _size_1 = l_forage.size();
        String _plus_1 = ("number of foraging locations: " + Integer.valueOf(_size_1));
        InputOutput.<String>println(_plus_1);
        final KeyMap<String, Forage> km_forage = new KeyMap<String, Forage>();
        for (final Forage f : l_forage) {
          km_forage.put(f.getId(), f);
        }
        final List<Bat> l_bats = new List<Bat>();
        IntegerRange _upTo_2 = new IntegerRange(1, (this.nbbats).intValue());
        for (final Integer b : _upTo_2) {
          {
            Integer id = b;
            double _random = Math.random();
            int _intValue = Double.valueOf((_random * 28)).intValue();
            int d = (1 + _intValue);
            final Bat bat = Bat.bat(id, Integer.valueOf(d));
            l_bats.add(bat);
          }
        }
        for (final Forage f_1 : l_forage) {
          for (final Roost r : l_roost) {
            {
              r.setNfmax(Double.valueOf(0d));
              double nf = 0d;
              final double d = f_1.getLocation().distance(r.getLocation());
              if ((d <= dmax)) {
                Double _fav = f_1.getFav();
                double _multiply = ((_fav).doubleValue() * (1 - (d / dmax)));
                nf = _multiply;
                Double _nfmax = r.getNfmax();
                boolean _greaterThan = (nf > (_nfmax).doubleValue());
                if (_greaterThan) {
                  r.setNfmax(Double.valueOf(nf));
                }
              }
              r.getKm_nf().put(Integer.valueOf(f_1.getId().substring(2)), Double.valueOf(nf));
            }
          }
        }
        List<Integer> lr0 = new List<Integer>();
        int i = 1;
        int r0 = i;
        for (i = 0; (i < l_bats.size()); i++) {
          {
            r0 = (i / 15);
            lr0.add(Integer.valueOf(r0));
          }
        }
        BatRoost r_batroost = new BatRoost();
        i = 0;
        for (final Bat b_1 : l_bats) {
          {
            int _plusPlus = i++;
            final Integer r0index = lr0.get(_plusPlus);
            final RandomUnique ru = new RandomUnique();
            final Integer r1index = ru.getValue(Integer.valueOf(nbroost));
            r_batroost.connect(b_1, l_roost.get((r0index).intValue()));
          }
        }
        r_batroost.choose();
        r_batroost.getRoostLocation();
        for (final Bat b_2 : l_bats) {
          b_2.returnRoost();
        }
        r_batroost.sumBats();
        for (final Roost r_1 : l_roost) {
          Integer _no_bats = r_1.getNo_bats();
          String _plus_2 = ("no. bats in roost = " + _no_bats);
          InputOutput.<String>println(_plus_2);
        }
        while (now.isBefore(end)) {
          {
            InputOutput.<String>println((("--- Date: " + now) + " ---"));
            r_batroost.batonroost().sumBats();
            for (final Roost r_2 : l_roost) {
              IntegerRange _upTo_3 = new IntegerRange(1, 1);
              for (final Integer ts : _upTo_3) {
                r_2.updateRoostpoint(now);
              }
            }
            for (final Bat b_3 : l_bats) {
              {
                b_3.setPosition(Point.xy(Double.valueOf(b_3.getRoost().getX()), Double.valueOf(b_3.getRoost().getY())));
                b_3.setActivity("roosting");
                b_3.updateTrack(now, now.addHours(12));
              }
            }
            r_batroost.monthlychange(Integer.valueOf(now.getDayOfMonth())).disconnect();
            for (final Bat b_4 : l_bats) {
              {
                Integer _no_daysRoost = b_4.getNo_daysRoost();
                int _minus = ((_no_daysRoost).intValue() - 1);
                b_4.setNo_daysRoost(Integer.valueOf(_minus));
                int _dayOfMonth = now.getDayOfMonth();
                Integer _dayChangeRoost = b_4.getDayChangeRoost();
                boolean _equals = (_dayOfMonth == (_dayChangeRoost).intValue());
                if (_equals) {
                  b_4.setNo_daysRoost(Integer.valueOf(0));
                  RandomUnique ru = new RandomUnique();
                  Integer _valueOf = Integer.valueOf(b_4.getRoost_id().substring(2));
                  int r0index = ((_valueOf).intValue() - 1);
                  Integer r1index = ru.getValue(Integer.valueOf(nbroost));
                  Integer r2index = ru.getValue(Integer.valueOf(nbroost));
                  Integer r3index = ru.getValue(Integer.valueOf(nbroost));
                  r_batroost.connect(b_4, l_roost.get(r0index));
                  if (((r1index).intValue() != r0index)) {
                    r_batroost.connect(b_4, l_roost.get((r1index).intValue()));
                  }
                  if (((r2index).intValue() != r0index)) {
                    r_batroost.connect(b_4, l_roost.get((r2index).intValue()));
                  }
                  if (((r3index).intValue() != r0index)) {
                    r_batroost.connect(b_4, l_roost.get((r3index).intValue()));
                  }
                }
                b_4.setForageTime(Integer.valueOf(0));
              }
            }
            r_batroost.validRoost().choose();
            r_batroost.getRoostLocation();
            for (final Roost r_3 : l_roost) {
              r_3.setNo_bats(Integer.valueOf(0));
            }
            r_batroost.batonroost().sumBats();
            now = now.addHours(12);
            for (final Bat b_5 : l_bats) {
              b_5.getPlForagelist().clear();
            }
            Specialrandoms sr = new Specialrandoms();
            r_batroost.batonroost().prepareForageList(l_forage, sr);
            Integer _id = l_bats.get(0).getId();
            String _plus_3 = ("Id bat: " + _id);
            String _plus_4 = (_plus_3 + "; List of plots for bat \'0\': ");
            List<String> _plForagelist = l_bats.get(0).getPlForagelist();
            String _plus_5 = (_plus_4 + _plForagelist);
            InputOutput.<String>println(_plus_5);
            final DateTime begin_of_night = now;
            final DateTime end_of_night = now.addHours(12);
            while (now.isBefore(end_of_night)) {
              {
                for (final Forage f_2 : l_forage) {
                  f_2.setNb_bats(Integer.valueOf(0));
                }
                for (final Bat b_6 : l_bats) {
                  {
                    Integer _forageTime = b_6.getForageTime();
                    boolean _lessEqualsThan = ((_forageTime).intValue() <= 0);
                    if (_lessEqualsThan) {
                      double _random = Math.random();
                      int _size_2 = b_6.getPlForagelist().size();
                      int iplot = Double.valueOf((_random * _size_2)).intValue();
                      Forage plotFood = km_forage.get(b_6.getPlForagelist().get(iplot));
                      do {
                        double _random_1 = Math.random();
                        int _size_3 = b_6.getPlForagelist().size();
                        iplot = Double.valueOf((_random_1 * _size_3)).intValue();
                      } while(Objects.equal(Integer.valueOf(iplot), b_6.getPlForage()));
                      b_6.setPlForage(plotFood.getId());
                      b_6.FoodTime();
                    }
                    b_6.distToRoost();
                    Integer _distRoost = b_6.getDistRoost();
                    double _divide = ((_distRoost).intValue() / speed);
                    b_6.setTimeToRoost(now.addMinutes(Long.valueOf(Math.round(_divide)).intValue()));
                    IntegerRange _upTo_4 = new IntegerRange(1, tsstep);
                    for (final Integer step : _upTo_4) {
                      {
                        boolean _isBefore = b_6.getTimeToRoost().isBefore(end_of_night);
                        if (_isBefore) {
                          double b_dx = b_6.getPosition().getX();
                          double b_dy = b_6.getPosition().getY();
                          Point plotFood_1 = km_forage.get(b_6.getPlForage()).getLocation();
                          double _x = plotFood_1.getX();
                          final double gap_x = (_x - b_dx);
                          double _y = plotFood_1.getY();
                          final double gap_y = (_y - b_dy);
                          double angle = Math.atan2(gap_y, gap_x);
                          b_6.setDistForage(Double.valueOf(b_6.getPosition().distance(plotFood_1)));
                          Double _distForage = b_6.getDistForage();
                          boolean _greaterThan = ((_distForage).doubleValue() > (speed * tsstep));
                          if (_greaterThan) {
                            double _cos = Math.cos(angle);
                            double _multiply = (speed * _cos);
                            double _plus_6 = (_multiply + b_dx);
                            b_dx = _plus_6;
                            double _sin = Math.sin(angle);
                            double _multiply_1 = (speed * _sin);
                            double _plus_7 = (_multiply_1 + b_dy);
                            b_dy = _plus_7;
                            b_6.setPosition(Point.xy(Double.valueOf(b_dx), Double.valueOf(b_dy)));
                            b_6.setDistForage(Double.valueOf(plotFood_1.distance(b_6.getPosition())));
                          } else {
                            b_6.setPosition(plotFood_1);
                          }
                          double _distance = b_6.getPosition().distance(plotFood_1);
                          boolean _lessEqualsThan_1 = (_distance <= 1);
                          if (_lessEqualsThan_1) {
                            b_6.setActivity("feeding");
                          } else {
                            b_6.setActivity("flying");
                          }
                        } else {
                          b_6.returnToRoost();
                        }
                        int min = now.getMinute();
                        boolean _contains = l_updt.contains(Integer.valueOf(min));
                        if (_contains) {
                          b_6.updateTrack(now, now.addMinutes(tsstep));
                        }
                      }
                    }
                    for (final Forage f_3 : l_forage) {
                      String _plForage = b_6.getPlForage();
                      String _id_1 = f_3.getId();
                      boolean _equals = Objects.equal(_plForage, _id_1);
                      if (_equals) {
                        Integer _nb_bats = f_3.getNb_bats();
                        int _plus_6 = ((_nb_bats).intValue() + 1);
                        f_3.setNb_bats(Integer.valueOf(_plus_6));
                      }
                    }
                    Forage PFood = km_forage.get(b_6.getPlForage());
                    double _distance = PFood.getLocation().distance(b_6.getRoost());
                    boolean _greaterThan = (_distance > dmax);
                    if (_greaterThan) {
                      InputOutput.<String>println("Distance NOT OK!");
                    }
                  }
                }
                int s = 0;
                for (final Forage f_3 : l_forage) {
                  Integer _nb_bats = f_3.getNb_bats();
                  boolean _greaterThan = ((_nb_bats).intValue() > 0);
                  if (_greaterThan) {
                    Integer _nb_bats_1 = f_3.getNb_bats();
                    int _plus_6 = (s + (_nb_bats_1).intValue());
                    s = _plus_6;
                  }
                }
                now = now.addMinutes(tsstep);
                for (final Bat b_7 : l_bats) {
                  Integer _forageTime = b_7.getForageTime();
                  int _minus = ((_forageTime).intValue() - tsstep);
                  b_7.setForageTime(Integer.valueOf(_minus));
                }
              }
            }
            for (final Bat b_6 : l_bats) {
              {
                final Batpoints bps = new Batpoints();
                List<Batpoint> _track = b_6.getTrack();
                String _substring = now.toString().substring(0, 10);
                String _plus_6 = ((SvPath + "/BatsShp/BatsShp_") + _substring);
                String _plus_7 = (_plus_6 + ".csv");
                bps.saveToCsv(_track, _plus_7);
              }
            }
          }
        }
        DateTime endTime = new DateTime();
        long _timeAsMilliseconds = endTime.getTimeAsMilliseconds();
        long _timeAsMilliseconds_1 = startTime.getTimeAsMilliseconds();
        long runTime_ms = (_timeAsMilliseconds - _timeAsMilliseconds_1);
        InputOutput.<String>println((((("Total running time: " + Long.valueOf(runTime_ms)) + " milliseconds, for ") + this.nbdays) + "d-simulation"));
        InputOutput.<String>println("End iteration");
      }
    }
    InputOutput.<String>println("End");
  }
  
  public void simulate(final HashMap<String, Object> in_params) {
    String val_wd = (String) in_params.get("wd");
    if (val_wd != null) wd = val_wd;
    String val_filename = (String) in_params.get("filename");
    if (val_filename != null) filename = val_filename;
    Integer val_nbsimul = (Integer) in_params.get("nbsimul");
    if (val_nbsimul != null) nbsimul = val_nbsimul;
    Integer val_nbbats = (Integer) in_params.get("nbbats");
    if (val_nbbats != null) nbbats = val_nbbats;
    Integer val_nbdays = (Integer) in_params.get("nbdays");
    if (val_nbdays != null) nbdays = val_nbdays;
    String val_startDate = (String) in_params.get("startDate");
    if (val_startDate != null) startDate = val_startDate;
    Integer val_outstep = (Integer) in_params.get("outstep");
    if (val_outstep != null) outstep = val_outstep;
    run_BatMove();
  }
  
  private String wd;
  
  public void setWd(final String wd) {
    this.wd = wd;
  }
  
  public String getWd() {
    return this.wd;
  }
  
  private String filename;
  
  public void setFilename(final String filename) {
    this.filename = filename;
  }
  
  public String getFilename() {
    return this.filename;
  }
  
  private Integer nbsimul;
  
  public void setNbsimul(final Integer nbsimul) {
    this.nbsimul = nbsimul;
  }
  
  public Integer getNbsimul() {
    return this.nbsimul;
  }
  
  private Integer nbbats;
  
  public void setNbbats(final Integer nbbats) {
    this.nbbats = nbbats;
  }
  
  public Integer getNbbats() {
    return this.nbbats;
  }
  
  private Integer nbdays;
  
  public void setNbdays(final Integer nbdays) {
    this.nbdays = nbdays;
  }
  
  public Integer getNbdays() {
    return this.nbdays;
  }
  
  private String startDate;
  
  public void setStartDate(final String startDate) {
    this.startDate = startDate;
  }
  
  public String getStartDate() {
    return this.startDate;
  }
  
  private Integer outstep;
  
  public void setOutstep(final Integer outstep) {
    this.outstep = outstep;
  }
  
  public Integer getOutstep() {
    return this.outstep;
  }
}
