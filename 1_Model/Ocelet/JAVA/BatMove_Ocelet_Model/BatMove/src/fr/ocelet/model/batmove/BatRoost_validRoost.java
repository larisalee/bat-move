package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.runtime.relation.EdgeFilter;

@SuppressWarnings("all")
public class BatRoost_validRoost implements EdgeFilter<Bat, Roost> {
  public BatRoost_validRoost() {
    
  }
  
  public Boolean filter(final Bat bat, final Roost roost) {
    return Boolean.valueOf(((((bat.getNo_daysRoost()).intValue() == 0) && (roost.getLocation().distance(bat.getRoost()) <= 4000)) && ((roost.getNo_bats()).intValue() <= 15)));
  }
}
