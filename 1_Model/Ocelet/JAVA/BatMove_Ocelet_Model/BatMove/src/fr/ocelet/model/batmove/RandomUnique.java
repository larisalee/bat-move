package fr.ocelet.model.batmove;

import fr.ocelet.runtime.ocltypes.List;

@SuppressWarnings("all")
public class RandomUnique {
  private List<Integer> history;
  
  public void setHistory(final List<Integer> history) {
    this.history = history;
  }
  
  public List<Integer> getHistory() {
    return this.history;
  }
  
  public Integer getValue(final Integer max) {
    int n = (-1);
    do {
      double _random = Math.random();
      n = Double.valueOf((_random * (max).intValue())).intValue();
    } while(this.history.contains(Integer.valueOf(n)));
    this.history.add(Integer.valueOf(n));
    return Integer.valueOf(n);
  }
  
  public RandomUnique() {
    super();
    history = new List<Integer>();
  }
}
