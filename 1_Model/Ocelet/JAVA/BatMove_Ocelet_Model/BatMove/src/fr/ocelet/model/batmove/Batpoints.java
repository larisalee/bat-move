package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Batpoint;
import fr.ocelet.model.batmove.Bats_shp;
import fr.ocelet.model.batmove.ShpBats;
import fr.ocelet.runtime.ocltypes.List;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class Batpoints {
  public void saveToCsv(final List<Batpoint> l_batp, final String csvfilename) {
    int _size = l_batp.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      final List<Bats_shp> l_batsshp = new List<Bats_shp>();
      final ShpBats shpbats = new ShpBats();
      shpbats.remove();
      shpbats.setFileName(csvfilename);
      int _size_1 = l_batp.size();
      boolean _greaterEqualsThan = (_size_1 >= 1);
      if (_greaterEqualsThan) {
        int _size_2 = l_batp.size();
        int _minus = (_size_2 - 1);
        IntegerRange _upTo = new IntegerRange(1, _minus);
        for (final Integer i : _upTo) {
          {
            final Batpoint bpa = l_batp.get(((i).intValue() - 1));
            final Bats_shp bats_shp = new Bats_shp();
            bats_shp.setLat(Integer.valueOf(Long.valueOf(Math.round(bpa.getGeom().getY())).intValue()));
            bats_shp.setLon(Integer.valueOf(Long.valueOf(Math.round(bpa.getGeom().getX())).intValue()));
            bats_shp.setGid(i);
            bats_shp.setDateShow(bpa.getTimestamp1().toString());
            bats_shp.setDateHide(bpa.getTimestamp2().toString());
            bats_shp.setId(bpa.getId());
            bats_shp.setRoost_id(bpa.getRoost_id());
            bats_shp.setForage_id(bpa.getForageID());
            bats_shp.setActivity(bpa.getActivity());
            l_batsshp.add(bats_shp);
          }
        }
        int _size_3 = l_batp.size();
        int _minus_1 = (_size_3 - 1);
        final Batpoint lastbp = l_batp.get(_minus_1);
        final Bats_shp bats_shp = new Bats_shp();
        bats_shp.setLat(Integer.valueOf(Long.valueOf(Math.round(lastbp.getGeom().getY())).intValue()));
        bats_shp.setLon(Integer.valueOf(Long.valueOf(Math.round(lastbp.getGeom().getX())).intValue()));
        bats_shp.setGid(Integer.valueOf(l_batp.size()));
        bats_shp.setDateShow(lastbp.getTimestamp1().toString());
        bats_shp.setDateHide(lastbp.getTimestamp2().toString());
        bats_shp.setId(lastbp.getId());
        bats_shp.setRoost_id(lastbp.getRoost_id());
        bats_shp.setForage_id(lastbp.getForageID());
        bats_shp.setActivity(lastbp.getActivity());
        l_batsshp.add(bats_shp);
      }
      shpbats.append(l_batsshp);
    }
    l_batp.clear();
  }
  
  public Batpoints() {
    super();
  }
}
