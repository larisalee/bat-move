package fr.ocelet.model.batmove;

import fr.ocelet.datafacer.InputDataRecord;
import fr.ocelet.datafacer.ocltypes.Csvfile;
import fr.ocelet.model.batmove.Roost_shp;
import fr.ocelet.runtime.entity.Entity;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.LinkedHashMap;

@SuppressWarnings("all")
public class ShpRoost extends Csvfile {
  public ShpRoost() {
    super("output/Roosts.csv");
  }
  
  public List<Roost_shp> readAllRoost_shp() {
    List<Roost_shp> _elist = new List<Roost_shp>();
    for (InputDataRecord _record : this) {
      _elist.add(createRoost_shpFromRecord(_record));
     }
    resetIterator();
    return _elist;
  }
  
  public List<Roost_shp> readAll() {
    return readAllRoost_shp();
  }
  
  public KeyMap<Object, Roost_shp> readToKeyMap(final String keyproperty) {
    KeyMap<Object,Roost_shp> _edf_km = new KeyMap<Object,Roost_shp>();
    for (InputDataRecord _record : this) {
    	Roost_shp _en_ti_ty = createRoost_shpFromRecord(_record);
    	Object _the_key_ = _en_ti_ty.getProperty(keyproperty);
      _edf_km.put(_the_key_,_en_ti_ty);
    }
    resetIterator();
    return _edf_km;
  }
  
  public Roost_shp createRoost_shpFromRecord(final InputDataRecord _rec) {
                      	    Roost_shp _entity = new Roost_shp();
    _entity.setProperty("dateShow",readString("start"));
    _entity.setProperty("dateHide",readString("end"));
    _entity.setProperty("roost_id",readString("roost_id"));
    _entity.setProperty("lon",readInteger("lon"));
    _entity.setProperty("lat",readInteger("lat"));
    _entity.setProperty("nb_bats",readInteger("nb_bats"));
    return _entity;
  }
  
  public LinkedHashMap<String, String> getMatchdef() {
    LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
    hm.put("start","java.lang.String");
    hm.put("end","java.lang.String");
    hm.put("roost_id","java.lang.String");
    hm.put("lon","java.lang.Integer");
    hm.put("lat","java.lang.Integer");
    hm.put("nb_bats","java.lang.Integer");
    return hm;
  }
  
  public String headerString() {
    StringBuffer sb = new StringBuffer();
    sb.append("start");
    sb.append(separator);                     
    sb.append("end");
    sb.append(separator);                     
    sb.append("roost_id");
    sb.append(separator);                     
    sb.append("lon");
    sb.append(separator);                     
    sb.append("lat");
    sb.append(separator);                     
    sb.append("nb_bats");
    return sb.toString();
  }
  
  public String propsString(final Entity _entity) {
    StringBuffer sb = new StringBuffer();
    sb.append(_entity.getProperty("dateShow").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("dateHide").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("roost_id").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("lon").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("lat").toString());
    sb.append(separator);                     
    sb.append(_entity.getProperty("nb_bats").toString());
    return sb.toString();
  }
}
