package fr.ocelet.model.batmove;

import fr.ocelet.datafacer.InputDataRecord;
import fr.ocelet.datafacer.OutputDataRecord;
import fr.ocelet.datafacer.ocltypes.Shapefile;
import fr.ocelet.model.batmove.Forage;
import fr.ocelet.runtime.entity.Entity;
import fr.ocelet.runtime.ocltypes.KeyMap;
import fr.ocelet.runtime.ocltypes.List;
import java.util.LinkedHashMap;

@SuppressWarnings("all")
public class Foraging extends Shapefile {
  public Foraging() {
    super("data/foraging.shp","EPSG:32629");
  }
  
  public List<Forage> readAllForage() {
    List<Forage> _elist = new List<Forage>();
    for (InputDataRecord _record : this) {
      _elist.add(createForageFromRecord(_record));
     }
    resetIterator();
    return _elist;
  }
  
  public List<Forage> readAll() {
    return readAllForage();
  }
  
  public KeyMap<Object, Forage> readToKeyMap(final String keyproperty) {
    KeyMap<Object,Forage> _edf_km = new KeyMap<Object,Forage>();
    for (InputDataRecord _record : this) {
    	Forage _en_ti_ty = createForageFromRecord(_record);
    	Object _the_key_ = _en_ti_ty.getProperty(keyproperty);
      _edf_km.put(_the_key_,_en_ti_ty);
    }
    resetIterator();
    return _edf_km;
  }
  
  public Forage createForageFromRecord(final InputDataRecord _rec) {
                      	    Forage _entity = new Forage();
    _entity.setProperty("location",readPoint("the_geom"));
    _entity.setProperty("id",readString("ID"));
    _entity.setProperty("fav",readDouble("Suit"));
    return _entity;
  }
  
  public LinkedHashMap<String, String> getMatchdef() {
    LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
    hm.put("the_geom","fr.ocelet.runtime.geom.ocltypes.Point");
    hm.put("ID","java.lang.String");
    hm.put("Suit","java.lang.Double");
    return hm;
  }
  
  public List<Forage> readFilteredForage(final String _filt) {
    setFilter(_filt);
    return readAllForage();
  }
  
  public OutputDataRecord createRecord(final Entity ety) throws IllegalArgumentException {
    OutputDataRecord odr = createOutputDataRec();
    if (odr != null) {
    odr.setAttribute("the_geom",((Forage) ety).getLocation());
    odr.setAttribute("ID",((Forage) ety).getId());
    odr.setAttribute("Suit",((Forage) ety).getFav());
    }
    return odr;
  }
}
