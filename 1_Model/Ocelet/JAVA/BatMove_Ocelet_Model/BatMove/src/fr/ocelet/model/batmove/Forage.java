package fr.ocelet.model.batmove;

import fr.ocelet.runtime.entity.AbstractEntity;
import fr.ocelet.runtime.entity.Hproperty;
import fr.ocelet.runtime.geom.ocltypes.Point;

@SuppressWarnings("all")
public class Forage extends AbstractEntity {
  public void setId(final String id) {
    setProperty("id",id);
  }
  
  public String getId() {
    return getProperty("id");
  }
  
  public void setLocation(final Point location) {
    setProperty("location",location);
  }
  
  public Point getLocation() {
    return getProperty("location");
  }
  
  public void setFav(final Double fav) {
    setProperty("fav",fav);
  }
  
  public Double getFav() {
    return getProperty("fav");
  }
  
  public void setNb_bats(final Integer nb_bats) {
    setProperty("nb_bats",nb_bats);
  }
  
  public Integer getNb_bats() {
    return getProperty("nb_bats");
  }
  
  public Forage() {
    super();
    defProperty("id",new Hproperty<String>());
    setId(new String());
    defProperty("location",new Hproperty<Point>());
    setLocation(new Point());
    defProperty("fav",new Hproperty<Double>());
    setFav(new Double("0"));
    defProperty("nb_bats",new Hproperty<Integer>());
    setNb_bats(new Integer("0"));
  }
}
