package fr.ocelet.model.batmove;

import fr.ocelet.model.batmove.Batpoint;
import fr.ocelet.runtime.entity.AbstractEntity;
import fr.ocelet.runtime.entity.Hproperty;
import fr.ocelet.runtime.geom.ocltypes.Point;
import fr.ocelet.runtime.ocltypes.DateTime;
import fr.ocelet.runtime.ocltypes.List;

/**
 * Entity of individual bats
 * Each individual bat can move independently from other members of their group.
 * Individual bats return each day to roost
 */
@SuppressWarnings("all")
public class Bat extends AbstractEntity {
  public void setGid(final Integer gid) {
    setProperty("gid",gid);
  }
  
  public Integer getGid() {
    return getProperty("gid");
  }
  
  public void setId(final Integer id) {
    setProperty("id",id);
  }
  
  public Integer getId() {
    return getProperty("id");
  }
  
  public void setRoost_id(final String roost_id) {
    setProperty("roost_id",roost_id);
  }
  
  public String getRoost_id() {
    return getProperty("roost_id");
  }
  
  public void setRoost(final Point roost) {
    setProperty("roost",roost);
  }
  
  public Point getRoost() {
    return getProperty("roost");
  }
  
  public void setDistRoost(final Integer distRoost) {
    setProperty("distRoost",distRoost);
  }
  
  public Integer getDistRoost() {
    return getProperty("distRoost");
  }
  
  public void setNo_daysRoost(final Integer no_daysRoost) {
    setProperty("no_daysRoost",no_daysRoost);
  }
  
  public Integer getNo_daysRoost() {
    return getProperty("no_daysRoost");
  }
  
  public void setDayChangeRoost(final Integer dayChangeRoost) {
    setProperty("dayChangeRoost",dayChangeRoost);
  }
  
  public Integer getDayChangeRoost() {
    return getProperty("dayChangeRoost");
  }
  
  public void setTimeToRoost(final DateTime timeToRoost) {
    setProperty("timeToRoost",timeToRoost);
  }
  
  public DateTime getTimeToRoost() {
    return getProperty("timeToRoost");
  }
  
  public void setPosition(final Point position) {
    setProperty("position",position);
  }
  
  public Point getPosition() {
    return getProperty("position");
  }
  
  public void setDx(final Double dx) {
    setProperty("dx",dx);
  }
  
  public Double getDx() {
    return getProperty("dx");
  }
  
  public void setDy(final Double dy) {
    setProperty("dy",dy);
  }
  
  public Double getDy() {
    return getProperty("dy");
  }
  
  public void setPlForage(final String plForage) {
    setProperty("plForage",plForage);
  }
  
  public String getPlForage() {
    return getProperty("plForage");
  }
  
  public void setActivity(final String activity) {
    setProperty("activity",activity);
  }
  
  public String getActivity() {
    return getProperty("activity");
  }
  
  public void setPlForagelist(final List<String> plForagelist) {
    setProperty("plForagelist",plForagelist);
  }
  
  public List<String> getPlForagelist() {
    return getProperty("plForagelist");
  }
  
  public void setForageTime(final Integer forageTime) {
    setProperty("forageTime",forageTime);
  }
  
  public Integer getForageTime() {
    return getProperty("forageTime");
  }
  
  public void setDistForage(final Double distForage) {
    setProperty("distForage",distForage);
  }
  
  public Double getDistForage() {
    return getProperty("distForage");
  }
  
  public void setTrack(final List<Batpoint> track) {
    setProperty("track",track);
  }
  
  public List<Batpoint> getTrack() {
    return getProperty("track");
  }
  
  public static Bat bat(final Integer batid, final Integer dayofchange) {
    final Bat b = new Bat();
    b.setId(batid);
    b.setRoost_id("-1");
    b.setNo_daysRoost(Integer.valueOf(0));
    b.setDayChangeRoost(dayofchange);
    return b;
  }
  
  public void FoodTime() {
    double _random = Math.random();
    double _multiply = (_random * 510);
    int _intValue = Double.valueOf((_multiply - 48)).intValue();
    int _plus = (48 + _intValue);
    this.setForageTime(Integer.valueOf(_plus));
  }
  
  public void distToRoost() {
    this.setDistRoost(Integer.valueOf(Long.valueOf(Math.round(this.getPosition().distance(this.getRoost()))).intValue()));
  }
  
  public void returnRoost() {
    this.setPosition(this.getRoost());
  }
  
  public void returnToRoost() {
    final int speed = 330;
    int tsstep = 1;
    double bdx = this.getPosition().getX();
    double bdy = this.getPosition().getY();
    double _x = this.getRoost().getX();
    final double gap_x = (_x - bdx);
    double _y = this.getRoost().getY();
    final double gap_y = (_y - bdy);
    double angle = Math.atan2(gap_y, gap_x);
    int dR = 0;
    dR = (this.getDistRoost()).intValue();
    if ((dR > (speed * tsstep))) {
      double _cos = Math.cos(angle);
      double _multiply = (speed * _cos);
      double _plus = (_multiply + bdx);
      bdx = _plus;
      double _sin = Math.sin(angle);
      double _multiply_1 = (speed * _sin);
      double _plus_1 = (_multiply_1 + bdy);
      bdy = _plus_1;
      this.setPosition(Point.xy(Double.valueOf(bdx), Double.valueOf(bdy)));
      dR = Double.valueOf(this.getRoost().distance(this.getPosition())).intValue();
    } else {
      this.setPosition(this.getRoost());
    }
    double _distance = this.getPosition().distance(this.getRoost());
    boolean _lessEqualsThan = (_distance <= 1);
    if (_lessEqualsThan) {
      this.setActivity("roosting");
    } else {
      this.setActivity("flying");
    }
  }
  
  public void updateTrack(final DateTime tstamp1, final DateTime tstamp2) {
    final Batpoint bp = new Batpoint();
    bp.setGid(this.getGid());
    bp.setGeom(Point.xy(Double.valueOf(this.getPosition().getX()), Double.valueOf(this.getPosition().getY())));
    bp.setTimestamp1(tstamp1);
    bp.setTimestamp2(tstamp2);
    bp.setId(this.getId());
    bp.setRoost_id(this.getRoost_id());
    bp.setForageID(this.getPlForage());
    bp.setActivity(this.getActivity());
    this.getTrack().add(bp);
  }
  
  public Bat() {
    super();
    defProperty("gid",new Hproperty<Integer>());
    setGid(new Integer("0"));
    defProperty("id",new Hproperty<Integer>());
    setId(new Integer("0"));
    defProperty("roost_id",new Hproperty<String>());
    setRoost_id(new String());
    defProperty("roost",new Hproperty<Point>());
    setRoost(new Point());
    defProperty("distRoost",new Hproperty<Integer>());
    setDistRoost(new Integer("0"));
    defProperty("no_daysRoost",new Hproperty<Integer>());
    setNo_daysRoost(new Integer("0"));
    defProperty("dayChangeRoost",new Hproperty<Integer>());
    setDayChangeRoost(new Integer("0"));
    defProperty("timeToRoost",new Hproperty<DateTime>());
    setTimeToRoost(new DateTime());
    defProperty("position",new Hproperty<Point>());
    setPosition(new Point());
    defProperty("dx",new Hproperty<Double>());
    setDx(new Double("0"));
    defProperty("dy",new Hproperty<Double>());
    setDy(new Double("0"));
    defProperty("plForage",new Hproperty<String>());
    setPlForage(new String());
    defProperty("activity",new Hproperty<String>());
    setActivity(new String());
    defProperty("plForagelist",new Hproperty<List<String>>());
    setPlForagelist(new List<String>());
    defProperty("forageTime",new Hproperty<Integer>());
    setForageTime(new Integer("0"));
    defProperty("distForage",new Hproperty<Double>());
    setDistForage(new Double("0"));
    defProperty("track",new Hproperty<List<Batpoint>>());
    setTrack(new List<Batpoint>());
  }
}
