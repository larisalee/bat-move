package fr.ocelet.model.batmove;

import com.google.common.base.Objects;
import fr.ocelet.model.batmove.Bat;
import fr.ocelet.model.batmove.Roost;
import fr.ocelet.runtime.relation.EdgeFilter;

@SuppressWarnings("all")
public class BatRoost_monthlychange implements EdgeFilter<Bat, Roost> {
  private Integer dayOfMonth;
  
  public BatRoost_monthlychange(final Integer dayOfMonth) {
    this.dayOfMonth = dayOfMonth;
  }
  
  public Boolean filter(final Bat bat, final Roost roost) {
    Integer _dayChangeRoost = bat.getDayChangeRoost();
    return Boolean.valueOf(Objects.equal(this.dayOfMonth, _dayChangeRoost));
  }
}
