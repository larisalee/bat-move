# Convert rasters of land cover maps & save them as shapefiles
# This was done to use the shapefiles later for maps (as plotting raster takes longer)

# Working directory
wd <- "D:/Mes Donnees/EboHealth/bat-move/"
setwd(wd)

# Create directory
# dir.create(paste0(wd, "0_Init/RAW_shp"))

files <- list.files("0_Init/RAW")
nfiles <- length(files)

for(i in 1:nfiles) {
  
  file <- substr(files[i], 1, nchar(files[i])-4)
  
  dir.create(paste0(wd, "0_Init/RAW_shp/", file))
  
  r <- terra::rast(paste0(wd, "0_Init/RAW/", files[i]))
  poly <- terra::as.polygons(r)
  names(poly) <- "code"
  
  terra::writeVector(poly, filename = paste0(wd, "0_Init/RAW_shp/", file), filetype = 'ESRI Shapefile')
  
}
