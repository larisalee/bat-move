Individual-based modeling of bat populations 
========================================================================

## Description

## Initialisation

tous les fichiers de 0_init/raw doivent être des .tif avec le préfixe RastX (X de 1 à autant qu'on veut) et le préfixe ScenX (X de 1 à autant qu'on veut) si on veut inclure des scénarios. A minima il doit y avoir au moins un Rast1_...tif


## Contributors

- [Larisa Lee-Cruz](https://www.researchgate.net/profile/Larisa-Lee-Cruz)
- [Maxime Lenormand](https://www.maximelenormand.com/)
- [Annelise Tran](https://scholar.google.com/citations?user=5TvOVpkAAAAJ&hl=en)
- [Pascal Degenne](https://www.researchgate.net/profile/Pascal-Degenne)

## Citation

If you use this code, please cite:

WORKIN' ON IT

If you need help, find a bug, want to give us advice or feedback, please contact us!
You can reach us at maxime.lenormand[at]inrae.fr
