### This script allows to animate the movement of one bat ###

library(dplyr)
library(ggplot2)

# working directory
wd <- "D:/Mes Donnees/EboHealth"
setwd(wd)

# Map of study area colored by land use ####
SA <- terra::rast(paste0(wd,"/bat-move/0_Init/RAW/R1_forested.tif"))

LandUsecol<-c(
  '112'= "#FF0000",  #Urban areas
  '211'= "#FFFF73",  #Rain-fed crops and fallow
  '212'= "#FFFF00",  #Lowland crops 
  '222'= "#FFD373",  #Palms
  '241'= "#E6CC4D",  #Mosaic of forest and crops
  '311'= "#006600",  #Dense forest
  '312'= "#68CA00",  #Open forest
  '313'= "#43D400",  #Gallery forest
  '317'= "#67CF00",  #Degraded / secondary forest
  '321'= "#D4DEBB",  #Grassland
  '322'= "#BBEA8C",  #Shrubland
  '323'= "#A6C03C",  #Wooded savannah
  '324'= "#8CF266",  #Transition shrub formation
  '332'= "#CCCCCC",  #Bare rocks
  '411'= "#A6A6FF"   #Inland marshes
)


## plot raster with ggplot ####
 #(note that plotting the raster takes a bit of time)
SA_df <- as.data.frame(SA, xy = TRUE)     #convert raster to data frame
colnames(SA_df) <- c("x", "y", "value")

lu_lab <- c("Urban areas", "Rain-fed crops & fallow", "Lowland crops", "Palms", "Mosaic of forest & crops", "Dense forest", "Open forest", "Gallery forest", 
            "Degraded / secondary forest", "Grassland", "Shrubland", "Wooded savannah", "Transition shrub formation", "Bare rocks", "Inland marshes")

p <- ggplot(SA_df, aes(x = x, y = y, fill = as.factor(value))) +
        geom_tile() +
        scale_fill_manual(values = LandUsecol, name = "Land cover", labels = lu_lab) +
        coord_equal() +
        theme_minimal()

## plot shapefile (instead of raster) ####
 #(plots faster than raster, but with lower quality)
SA_shp <- sf::st_read(paste0(wd,"/bat-move/0_Init/study_area_shp/Macenta_clip_Palia_large.shp"))

LUmap <- ggplot()+
   geom_sf(data = SA_shp, aes(fill = code2015), alpha = 0.5, color=NA)+
   scale_colour_manual(values = LandUsecol, guide = 'none')+
   scale_fill_manual(values = LandUsecol, name="Land use",
                    labels=lu_lab)


## Read roost locations ####
Roost_shp <- sf::st_read(paste0(wd, "/bat-move/0_Init/SHPs/Roost/R1_forested.shp"))

# Process bat movement data ####

## Load data
pathS <- paste0(wd, "/bat-move/1_Model/Outputs/R1_forested/Sim_1/BatsShp/") #path to data for one simulation

## Load csv files
listb <- list.files(pathS)  #list of csv files
bats <- list()  #empty list to put dataframes

## Process data frames to output movement of one bat
for (i in 1:length(listb)) {
  
  bt <- read.csv(paste0(pathS, listb[i]), sep = ";")
  bt <- subset(bt, id_bat == 1)   #subset to only first bat
  bt <- bt[-c(1)]             #remove unneeded columns
  bt$start <- as.POSIXct(bt$start)
  bt$dist <- sp::spDistsN1(pts = matrix(c(bt[,7], bt[,8]), nrow = nrow(bt), ncol=2, byrow=F), pt=c(bt[1,7],bt[1,8]), longlat = T) #distance
  bt$speed <- bt$dist/1200  #speed, 1200 = 20*60 (as locations are spaced every 20 min)
  bats[[i]] <- bt
  
}

batsDF<-do.call(rbind,bats) #combine all data frames into one
movBats <- batsDF

movBats<-movBats %>%
  mutate(lon_nxt = lead(lon),
         lat_nxt = lead(lat))

## PLOT without animation ####
(pbat <- LUmap +

  #plot roosts
  geom_sf(data = Roost_shp, color = 'darkred', shape = 18, size = 2) + 
  
  # lines and points
  geom_path(data = movBats,
            aes(x = lon, y = lat), 
                alpha = 0.3) +
  
  geom_point(data = movBats, 
             aes(x = lon, y = lat), color = "black", fill = "lightblue",
             alpha = 0.7, shape = 21, size = 1))

## ANIMATION ####
# (Animations below work, but they still need tweaking to make them more fluid)
# (Maybe the best would be to get an output from Ocelet more frequent than 20 min ?)

p_anim <- ggplot(color = factor(movBats$id_bat)) +
 
  # land cover map
  geom_sf(data = SA_shp, aes(fill = code2015), alpha = 0.5, color=NA)+
  scale_colour_manual(values = LandUsecol, guide = 'none')+
  scale_fill_manual(values = LandUsecol, name="Land use", labels=lu_lab) +
  
  #plot roosts
  geom_sf(data = Roost_shp, color = 'darkred', shape = 18, size = 2) + 
  
  # lines and points
  geom_path(data = movBats,
            aes(x = lon, y = lat),
            alpha = 0.3) +
  geom_point(data = movBats, 
             aes(x = lon, y = lat), color = "black", fill = "lightblue",
             alpha = 0.7, shape = 21, size = 1)

# Animation

#path to save animations
pathO <- paste0(wd, "/bat-move/4_Animation/")

## Animation_1
anim <- p_anim +
  gganimate::transition_reveal(along = start)+ #, keep_last = F)+
  gganimate::ease_aes('linear') +
  gganimate::shadow_trail(alpha=0.3) +
  gganimate::enter_appear() +
  gganimate::exit_fade()
  ggtitle("Date: {frame_along}")

anim <- p_anim +
  gganimate::transition_events(start = start, end = end, enter_length = 1, exit_length = 2)+ #, keep_last = F)+
  gganimate::ease_aes('linear') +
  gganimate::shadow_trail(alpha=0.3) +
  ggtitle("Date: {frame_along}")

AA <- gganimate::animate(anim, nframes=200, fps=2, renderer = gganimate::gifski_renderer())
gganimate::anim_save("Bat_forested",AA, path = pathO, type='GIF')

## Animation_2
p_anim2 <- ggplot(color = factor(movBats$id_bat)) +
  
  # land cover map
  geom_sf(data = SA_shp, aes(fill = code2015), alpha = 0.5, color=NA)+
  scale_colour_manual(values = LandUsecol, guide = 'none')+
  scale_fill_manual(values = LandUsecol, name="Land use", labels=lu_lab) +
  
  #plot roosts
  geom_sf(data = Roost_shp, color = 'darkred', shape = 18, size = 2) + 
  
  #segments & points
  geom_segment(data = movBats,
            aes(x=lon, y=lat, group=factor(id_bat),#,color=factor(id_bat)),
                xend = lon_nxt,
                yend = lat_nxt),
                alpha = 0.3) +
  
  geom_point(data = movBats, 
             aes(x = lon, y = lat), color = "black", fill = "lightblue",
             alpha = 0.7, shape = 21, size = 1)

anim2 = p_anim2 +
  gganimate::transition_components(as.numeric(start), enter_length=30, exit_length = 30) +
  # gganimate::ease_aes(x = 'sine-out', y = 'sine-out') +
  gganimate::ease_aes(x = 'linear', y = 'linear') +
  # gganimate::shadow_wake(0.05, size = 2, alpha = TRUE, wrap = FALSE, falloff = 'sine-in', exclude_phase = 'exit') +
  gganimate::shadow_wake(0.05, size = 1, alpha = TRUE, wrap = FALSE, falloff = 'circular-out', exclude_phase = NULL) +
  ggtitle("Date: {frame_along}")

AB <- gganimate::animate(anim2, nframes=200, fps=2, renderer = gganimate::gifski_renderer())
gganimate::anim_save("Bat_forested2",AB, path = pathO, type='GIF')


