# Estimate mean & sd of density of in-coming bats around urban areas for each scenario

library(ggplot2)

# Working directory
wd <- "D:/Mes Donnees/EboHealth/bat-move"
setwd(wd)

dat <- read.csv2("2_Outputs/Density_urban/Density_urbanAreas.csv")
files <- unique(dat$File)
nbfiles <- length(files)

DF <- NULL

for(i in 1:nbfiles){  # Loop over the files
  
  scen <- files[i]
  dens <- subset(dat, dat$File == scen)
  
  mean <- mean(dens$Density)
  sd <- sd(dens$Density)

  
  df <- data.frame(scen, mean, sd)
  DF <- rbind(DF, df)
  
  print(paste0(scen, ": ", mean))

}

write.csv2(DF, "3_Analysis/MeanDensity_urbanAreas.csv", row.names = FALSE)

# Plot
p <- ggplot(data = DF) +
    geom_errorbar(aes(x = scen, ymin = mean - sd, ymax = mean + sd), width = 0.4, color = 'darkgrey', linewidth = 0.8) +
    geom_bar(aes(x = scen, y = mean), stat = "identity", fill = "lightgrey") +
    labs(x = "Scenario", y = bquote(Density~ (bats/km^2))) +
    scale_x_discrete(labels=c('Forested', '2015', 'Fragm.', 'Deforest.')) +
    scale_y_continuous(limits = c(0, 400), expand = expansion(mult = c(0, .1))) +
    theme(axis.line = element_line(),
        panel.background = element_blank(), 
        axis.text = element_text(size = 14, colour = "grey30"),
        axis.title = element_text(size = 16, colour = "grey30"),
        panel.grid.major.y = element_line(colour = 'lightgrey', linetype = 'solid'),
        panel.grid.major.x = element_line(colour = 'lightgrey', linetype = 'solid'))

x11(width = 6, height = 4.5)
ggsave(plot = p, filename = "Density_urbanAreas.png", path = paste0(wd, "/5_Plots"), bg = "white", dpi = 300)


