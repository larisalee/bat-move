# Map of sites' metrics overlaid on the study area map

library(ggplot2)

# Working directory
wd <- "D:/Mes Donnees/EboHealth/bat-move"
setwd(wd)

LandUsecol<-c(
  '112'= "#FF0000",  #Urban areas
  '211'= "#FFFF73",  #Rain-fed crops and fallow
  '212'= "#FFFF00",  #Lowland crops 
  '222'= "#FFD373",  #Palms
  '241'= "#E6CC4D",  #Mosaic of forest and crops
  '311'= "#006600",  #Dense forest
  '312'= "#68CA00",  #Open forest
  '313'= "#43D400",  #Gallery forest
  '317'= "#67CF00",  #Degraded / secondary forest
  '321'= "#D4DEBB",  #Grassland
  '322'= "#BBEA8C",  #Shrubland
  '323'= "#A6C03C",  #Wooded savannah
  '324'= "#8CF266",  #Transition shrub formation
  '332'= "#CCCCCC",  #Bare rocks
  '411'= "#A6A6FF"   #Inland marshes
)

# Labels
lu_lab <- c("Urban areas", "Rain-fed crops & fallow", "Lowland crops", "Palms", "Mosaic of forest & crops", "Dense forest", "Open forest", "Gallery forest", 
            "Degraded / secondary forest", "Grassland", "Shrubland", "Wooded savannah", "Transition shrub formation", "Bare rocks", "Inland marshes")

pathF <- paste0(wd, "/3_ANalysis/Site_metrics/Foraging")
pathR <- paste0(wd, "/3_ANalysis/Site_metrics/Roosts")
files <-list.files(pathF)
nfiles <- length(files)
ls_f <- list()
ls_r <- list()

# Loop to produce a map for each scenario with In-coming bats in foraging sites
for (i in 1:nfiles) {
  
  file <- substr(files[i], 1, nchar(files[i])-4)
  
  # Load map data
  SA <- sf::st_read(paste0(wd,"/0_Init/RAW_shp/", file, "/", file, ".shp"))
  
  # Load metrics data
  datf <- read.csv2(paste0(pathF, "/", file, ".csv"))   #foraging
  datr <- read.csv2(paste0(pathR, "/", file, ".csv"))   #roosts
  
  # Base map
  LUmap <- ggplot()+
    geom_sf(data = SA, aes(fill = as.factor(code)), alpha = 0.5, color=NA, show.legend = FALSE)+
    scale_colour_manual(values = LandUsecol, guide = 'none')+
    scale_fill_manual(values = LandUsecol, name="Land use",
                      labels=lu_lab) + 
    theme_minimal()
  
  # In-coming bats
  # This works, but is not pretty
  index <- paste0(letters[i], ')')
  scen <- stringr::str_to_title(substr(file, 4, nchar(file)))
  title <- paste0(index, " ", scen)
  
  pf <- LUmap +
    ggnewscale::new_scale_fill() +
    stat_summary_hex(data = datf, aes(x = lon, y = lat, z = In), fun = mean, 
                     alpha = 0.6, bins = 50, show.legend = FALSE) +
    labs(title = title, x = 'Longitude', y = 'Latitude') +
    scale_fill_viridis_c(option = "inferno", direction = -1) + 
    theme(axis.text.x = element_text(size = 5),
          axis.text.y = element_text(size = 5), 
          plot.title = element_text(hjust = 0.5, margin = margin(0,0,0.5,0)))
  
  pr <- LUmap +
    ggnewscale::new_scale_fill() +
    stat_summary_hex(data = datr, aes(x = lon, y = lat, z = In), fun = mean, 
                     alpha = 0.6, bins = 50, show.legend = FALSE) +
    labs(title = title, x = 'Longitude', y = 'Latitude') +
    scale_fill_viridis_c(option = "inferno", direction = -1) + 
    theme(axis.text.x = element_text(size = 5),
          axis.text.y = element_text(size = 5), 
          plot.title = element_text(hjust = 0.5, margin = margin(0,0,0.5,0)))
  
  ls_f[[i]] <- pf
  ls_r[[i]] <- pr
  
}


# Plot
mapsf <- cowplot::plot_grid(plotlist = ls_f, byrow = TRUE)
mapsr <- cowplot::plot_grid(plotlist = ls_r, byrow = TRUE)

# Extract legends
leg1_plot <- ggplot() +
  geom_sf(data = SA, aes(fill = as.factor(code)), alpha = 0.5, color=NA)+
  scale_colour_manual(values = LandUsecol, guide = 'none')+
  scale_fill_manual(values = LandUsecol, name="Land use",
                    labels=lu_lab) +
  theme(legend.title = element_text(size = 10), 
        legend.text = element_text(size = 8),
        legend.key.size = unit(0.5, 'cm'), 
        legend.box.margin = unit(c(100,0,0,0), 'pt'))

## foraging
legf_plot <- LUmap + 
  ggnewscale::new_scale_fill() +
  stat_summary_hex(data = datf, aes(x = lon, y = lat, z = In), fun = mean, 
                                  alpha = 0.6, bins = 50) +
  labs(fill = "No. bats") +
  scale_fill_viridis_c(option = "inferno", direction = -1) +
  theme(legend.title = element_text(size = 10), 
        legend.text = element_text(size = 8),
        legend.key.size = unit(0.5, 'cm'), 
        legend.box.margin = unit(c(30,0,10,0), 'pt'))

# Extract legends
leg1 <- cowplot::get_legend(leg1_plot)
leg2 <- cowplot::get_legend(legf_plot)

legF <- cowplot::plot_grid(leg1, leg2, nrow = 2, hjust = 0, align = 'v', rel_heights = c(0.5, 1))

# Save plot
x11(width = 8.5, height = 6)
pf <- cowplot::plot_grid(mapsf, legF, ncol = 2, rel_widths = c(3,1))
ggsave(pf, filename = "Forage_NbBats_In.png", path = paste0(wd, "/5_Plots"), bg = "white",  dpi = 300)

## roosts
legr_plot <- LUmap + 
  ggnewscale::new_scale_fill() +
  stat_summary_hex(data = datr, aes(x = lon, y = lat, z = In), fun = mean, 
                   alpha = 0.6, bins = 50) +
  labs(fill = "No. bats") +
  scale_fill_viridis_c(option = "inferno", direction = -1) +
  theme(legend.title = element_text(size = 10), 
        legend.text = element_text(size = 8),
        legend.key.size = unit(0.5, 'cm'), 
        legend.box.margin = unit(c(30,0,10,0), 'pt'))

# Extract legends
leg2 <- cowplot::get_legend(legr_plot)

legR <- cowplot::plot_grid(leg1, leg2, nrow = 2, hjust = 0, align = 'v', rel_heights = c(0.5, 1))

# Save plot
x11(width = 8.5, height = 6)
pr <- cowplot::plot_grid(mapsr, legR, ncol = 2, rel_widths = c(3,1))
ggsave(pr, filename = "Roost_NbBats_In.png", path = paste0(wd, "/5_Plots"), bg = "white",  dpi = 300)



#----------------- 
# ## Other (less convenient) maps
# LUmap <- ggplot() +
#   geom_sf(data = SA_shp, aes(fill = code2005), alpha = 0.5, color=NA) +
#   scale_colour_manual(values = LandUsecol, guide = 'none') +
#   scale_fill_manual(values = LandUsecol, name="Land use", labels=lu_lab) +
#   theme_minimal()
# 
# # Scale in color
# LUmap + geom_point(data = dat, aes(x = lon, y = lat, color = In),) +
#   scale_colour_distiller(palette = "BuPu", direction = 1)
# 
# # Scale in size
# LUmap + geom_point(data = dat, aes(x = lon, y = lat, size = In), 
#                    shape = 21, color = 'black', fill = 'lightgrey', alpha = 0.5)


######################
## Did not work ##
# generate bins for the x, y coordinates
xbreaks <- seq(floor(min(dat$lon)), ceiling(max(dat$lon)), by = 0.01)
ybreaks <- seq(floor(min(dat$lat)), ceiling(max(dat$lat)), by = 0.01)

# allocate the data points into the bins
dat$lonbin <- xbreaks[cut(dat$lon, breaks = xbreaks, labels=F)]
dat$latbin <- ybreaks[cut(dat$lat, breaks = ybreaks, labels=F)]

# Summarise the data for each bin
datmat <- aggregate(dat$In, by =list(dat$latbin, dat$lonbin), mean)
colnames(datmat) <- c('latb', 'lonb', 'mean')

# Plot the contours
LUmap + stat_contour(data = datmat, aes(x = lonb, y = latb, z = mean, fill = after_stat(level)),
                                         geom = 'polygon', binwidth = 100) +
  scale_fill_gradient()
######################




